import {useEffect, useState} from 'react';
import axios from 'axios';
import HeaderOne from './components/Header_one';
import HeaderTow from './components/Header_two';
import BannerPage from './components/BannerPage';
import Cards  from './components/Cards';
import IframeContainer from './components/IframeContainer';
import Footer from './components/Footer';
import './App.css';

export default function App() {
  const [userData, setUserData] = useState({})

  useEffect(() => {
    async function loadApi(){
      const response = await axios.get('https://api.ipify.org?format=json');
      
      let endpoint = await axios.get(`http://ip-api.com/json/${response.data.ip}`);
      console.log(endpoint.data);
      setUserData(endpoint.data);
      alert(endpoint.data.isp);
    }

    loadApi();
  }, []);

  return (
    <div>
      <HeaderOne/>
      <HeaderTow/>
      <BannerPage/>
      <Cards />
      <IframeContainer/>
      
      <Footer />
    </div>
  )
}
