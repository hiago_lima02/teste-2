import {useState} from 'react';
import {BsSearch} from 'react-icons/bs';
import {GiHamburgerMenu} from 'react-icons/gi';
import {IoIosCloseCircleOutline, IoIosHand} from 'react-icons/io';
import {WiMoonAltThirdQuarter} from 'react-icons/wi';
import {AiOutlineZoomIn, AiOutlineZoomOut} from 'react-icons/ai';
import './estilo.css';

export default function HeaderTwo() {
  function handleMenu(val){
    if(val){
      document.querySelector('.menu-retratil').style.display = 'flex';
    } else {
      document.querySelector('.menu-retratil').style.display = 'none';
    }
  }
  return (
    <>
      <div className="content-menu menu-2 desktop">
        <img src="https://www.tim.com.br/themes/custom/timbrasil/logo.svg" alt="logo tim" />
        <nav>
          <a href="#">Planos e Serviços</a>
          <a href="#">Loja online</a>
          <a href="#">Por que TIM</a>
          <a href="#">Atendimento</a>
          <a href="#">Meu TIM</a>
          <a href="#"><BsSearch size={20}/></a>
        </nav>
      </div>

      <div className="content-menu mobile">
        <img src="https://www.tim.com.br/themes/custom/timbrasil/logo.svg" alt="logo tim" />
        <span onClick={() => handleMenu(true)}><GiHamburgerMenu size={30}/></span>
      </div>

      <div  className="menu-retratil">
        <div className="select sub-select">
          <label>Você está em:</label>
          <select>
            <option value="">Rio de Janeiro</option>
            <option value="">São Paulo</option>
            <option value="">Amapá</option>
          </select>
        </div>
        <div className="sub-left">
          <img src="https://www.tim.com.br/themes/custom/timbrasil/logo.svg" alt="logo tim" />
          <button>Para voce</button>
          <IoIosCloseCircleOutline color={'#fff'} size={50} onClick={() => handleMenu(false)} />
        </div>
        <ul>
          <li><a href="#">Planos e Serviços</a></li>
          <li><a href="#">Loja online</a></li>
          <li><a href="#">Por que TIM</a></li>
          <li><a href="#">Atendimento</a></li>
          <li><a href="#">Meu TIM</a></li>
        </ul>

        <div className="bottomIcons">
          <a href="#"><BsSearch size={20}/></a>
          <div className="acessibilidade">
            <a href="#">Acessibilidade</a>
          </div>
          <div className="right-icons">
            <a href="#"><IoIosHand size={20}/></a>
            <a href="#"><WiMoonAltThirdQuarter size={20}/></a>
            <a href="#"><AiOutlineZoomIn size={20}/></a>
            <a href="#"><AiOutlineZoomOut size={20}/></a>
          </div>
        </div>
      </div>
    </>
  )
}
