import React from 'react';
import {HiOutlineUserCircle, HiOutlineGlobeAlt} from 'react-icons/hi';
import './estilo.css';

export default function Cards() {
  return (
    <div className="cards-container">
      <div className="cards-top">
        <h4>Autoatendimento</h4>
        <h2>Acompanhe aqui o seu processo de portabilidade</h2>
      </div>

      <div className="cards">
        <div className="card card-01">
          <HiOutlineUserCircle size={24} />
          <span>Pessoa física</span>
        </div>
        <div className="card card-02">
          <HiOutlineGlobeAlt size={24} />
          <span>Pessoa jurídica</span>
        </div>
      </div>
    </div>
  )
}
