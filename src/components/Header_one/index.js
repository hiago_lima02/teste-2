import React from 'react';
import {IoIosHand} from 'react-icons/io';
import {WiMoonAltThirdQuarter} from 'react-icons/wi';
import {AiOutlineZoomIn, AiOutlineZoomOut} from 'react-icons/ai';
import './estilo.css';

export default function HeaderOne() {
  return (
    <div className="content-menu" style={{borderBottom: '1px solid #1B38A3'}}>
      <nav className="left">
        <a href="#">Para Você</a>
        <a href="#">Para Empresas</a>
        <a href="#">Para Operadora</a>
        <a href="#">Quem Somos</a>
        <a href="#">Acessar Meu TIM</a>
      </nav>
      <div className="select">
        <label>Você esta: </label>
        <select>
          <option>Rio de Janeiro</option>
          <option>São Paulo</option>
        </select>
      </div>
      <div className="acessibilidade">
        <a href="#">Acessibilidade</a>
      </div>
      <div className="right-icons">
        <a href="#"><IoIosHand size={20}/></a>
        <a href="#"><WiMoonAltThirdQuarter size={20}/></a>
        <a href="#"><AiOutlineZoomIn size={20}/></a>
        <a href="#"><AiOutlineZoomOut size={20}/></a>
      </div>
    </div>
  )
}
