import React from 'react';
import './estilo.css';
import imgFooter from '../../assets/img-02.png';
import logo from '../../assets/logo_0_1.png';
import {BsApple} from 'react-icons/bs';
import {FaGooglePlay} from 'react-icons/fa';

export default function Footer() {
  return (
    <footer>
      <div className="footer-left">
        <ul>
          <h3>Para você</h3>
          <li>Planos</li>
          <li>Internet</li>
          <li>Recarga</li>
          <li>Portabilidade</li>
          <li>Loja online</li>
          <li>Mapa de cobertura</li>
          <li>Plano e ofertas</li>
        </ul>
        <ul>
          <h3>Atendimento</h3>
          <li>2ª via de conta</li>
          <li>Perguntas frequentes</li>
          <li>Acessar Meu TIM</li>
          <li>Deficientes Auditivos</li>
          <li>Cancelamento de linha</li>
          <li>Cancelamento de linha corp</li>
          <li>Consulta a Direito a Devolução</li>
          <li>Dicas para uso de dados</li>
        </ul>
        <ul>
          <h3>Sobre a TIM</h3>
          <li>Institucional</li>
          <li>Trabalhe conosco</li>
          <li>Instituto TIM</li>
          <li>Regulatório</li>
          <li>Privacidade</li>
          <li>Sala de Imprensa</li>
          <li>Conselho de Usuários</li>
          <li>Relação com investidores</li>
          <li>Pesquisa de satisfação e qualidade percebida</li>
          <li>Termo de ajustamento de conduta - TAC</li>
        </ul>
        <ul>
          <h3>Fale conosco</h3>
          <li>Canais de atendimento</li>
          <li>Encontre uma loja</li>
          <li>Para empresas</li>
          <li>Seja um lojista</li>
          <li>Seja um parceiro</li>
          <li>International Visitors</li>
        </ul>
      </div>
      <div className="footer-right" style={{backgroundImage: `url(${imgFooter})`}} >
        <img src={logo} />
        <h3>App Meu TIM</h3>
        <p>Aproveite os benefícios e a comodidade do app Meu TIM!</p>
        <div>
          <BsApple className="marginIcon" size={35} />
          <FaGooglePlay size={35} />
        </div>
      </div>
    </footer>
  )
}
