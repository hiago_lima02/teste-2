import React from 'react';
import img from '../../assets/img-01.png';
import './estilo.css';

export default function BannerPage() {
  return (
    <div className="banner-container">
      <h2>Consulte para saber se um número é ou não TIM</h2>
      <img src={img} />
    </div>
  )
}
